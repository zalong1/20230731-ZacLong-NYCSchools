//
//  NYCSchoolData.swift
//  NYCSchoolListTest
//
//  Created by Zac Long on 7/30/23.
//

import Foundation

class NYCSchoolAsyncListModel: AsyncListModel {

    var data: [ListTableCellData] = [NYCSchoolData]()

    let sourceURL = "https://data.cityofnewyork.us/resource/s3k6-pzi2.json"
    let satSourceURL = "https://data.cityofnewyork.us/resource/f9bf-2cp4.json"

    weak var delegate: AsyncListModelDelegate?

    private(set) var state: AsyncListModelState = .hasNotTriedToLoad

    private var schoolDataByDBN = [String: NYCSchoolData]()

    var listTitle: String = "NYC School List"

    func getDataAsync() {
        state = .loading
        getSchoolData()
    }

    private func getSchoolData() {
        guard let url = URL(string: sourceURL) else{
            return
        }
        schoolDataByDBN = [String: NYCSchoolData]()
        let apiTask = URLSession.shared.dataTask(with: url){ [weak self] data, response, error in
            if let jsonData = data {
                let decoder = JSONDecoder()
                do {
                    let rawData = try decoder.decode([NYCSchoolAPIData].self, from: jsonData)
                    for school in rawData {
                        self?.schoolDataByDBN[school.dbn] = NYCSchoolData(schoolAPIData: school)
                    }
                    self?.getSATData()
                } catch(let decodeError) {
                    print("Error Decoding JSON: \(decodeError)")
                }
            } else {
                print("API Error for School Data: \(String(describing: error))")
            }
        }
        apiTask.resume()
    }

    private func getSATData() {
        guard let url = URL(string: satSourceURL) else{
            return
        }
        let apiTask = URLSession.shared.dataTask(with: url){ [weak self] data, response, error in
            if let jsonData = data {
                let decoder = JSONDecoder()
                do {
                    let rawData = try decoder.decode([NYCSchoolSATAPIData].self, from: jsonData)
                    for school in rawData {
                        if let schoolData = self?.schoolDataByDBN[school.dbn] {
                            schoolData.sat_math_avg_score = school.sat_math_avg_score
                            schoolData.sat_writing_avg_score = school.sat_writing_avg_score
                            schoolData.sat_critical_reading_avg_score = school.sat_critical_reading_avg_score
                            self?.schoolDataByDBN[schoolData.dbn] = schoolData
                        }
                    }
                } catch(let decodeError) {
                    print("Error Decoding JSON: \(decodeError)")
                }
            } else {
                print("API Error for SAT Data: \(String(describing: error))")
            }
            self?.dataFinishedLoading()
        }
        apiTask.resume()
    }

    private func dataFinishedLoading() {
        state = .dataLoaded
        data = Array(schoolDataByDBN.values)
        DispatchQueue.main.async { [weak self] in
            self?.delegate?.dataUpdated()
        }
    }
}

struct NYCSchoolAPIData: Codable {
    var dbn: String
    var school_name: String
    var boro: String?
    var overview_paragraph: String?
    var school_10th_seats: String?
    var academicopportunities1: String?
    var academicopportunities2: String?
    var academicopportunities3: String?
    var academicopportunities4: String?
    var ell_programs: String?
    var language_classes: String?
    var advancedplacement_courses: String?
    var neighborhood: String?
    var shared_space: String?
    var campus_name: String?
    var building_code: String?
    var location: String?
    var phone_number: String?
    var fax_number: String?
    var school_email: String?
    var website: String?
    var subway: String?
    var bus: String?
}

struct NYCSchoolSATAPIData: Codable {
    var dbn: String
    var school_name: String
    var num_of_sat_test_takers: String?
    var sat_critical_reading_avg_score: String?
    var sat_math_avg_score: String?
    var sat_writing_avg_score: String?
}

class NYCSchoolData: ListTableCellData {
    var title: String {
        school_name
    }

    var subTitle: String {
        website ?? ""
    }

    var dbn: String
    var school_name: String
    var overview_paragraph: String?
    var phone_number: String?
    var fax_number: String?
    var school_email: String?
    var website: String?
    var sat_critical_reading_avg_score: String?
    var sat_math_avg_score: String?
    var sat_writing_avg_score: String?

    init(dbn: String, school_name: String, overview_paragraph: String? = nil, phone_number: String? = nil, fax_number: String? = nil, school_email: String? = nil, website: String? = nil, sat_critical_reading_avg_score: String? = nil, sat_math_avg_score: String? = nil, sat_writing_avg_score: String? = nil) {
        self.dbn = dbn
        self.school_name = school_name
        self.overview_paragraph = overview_paragraph
        self.phone_number = phone_number
        self.fax_number = fax_number
        self.school_email = school_email
        self.website = website
        self.sat_critical_reading_avg_score = sat_critical_reading_avg_score
        self.sat_math_avg_score = sat_math_avg_score
        self.sat_writing_avg_score = sat_writing_avg_score
    }

    convenience init(schoolAPIData: NYCSchoolAPIData) {
        self.init(dbn: schoolAPIData.dbn,
                  school_name: schoolAPIData.school_name,
                  overview_paragraph: schoolAPIData.overview_paragraph,
                  phone_number: schoolAPIData.phone_number,
                  fax_number: schoolAPIData.fax_number,
                  school_email: schoolAPIData.school_email,
                  website: schoolAPIData.website)
    }
}
