//
//  AsyncListTableViewController.swift
//  NYCSchoolListTest
//
//  Created by Zac Long on 7/30/23.
//

import Foundation
import UIKit

protocol ListTableCellData {
    var title: String { get }
    var subTitle: String { get }
    // Subdata as well, perhaps
}

struct BasicListTableCellData: ListTableCellData {
    var title: String
    var subTitle: String
}

enum AsyncListModelState {
    case hasNotTriedToLoad
    case loading
    case errorLoading
    case dataLoaded
}

protocol AsyncListModelDelegate: AnyObject {
    // Could have the model pass back the data and/or state here, for this it'll just live in the model
    func dataUpdated()
}

protocol AsyncListModel {
    // Start getting data asynchronously
    func getDataAsync()
    // Data for the cells
    var data: [ListTableCellData] { get }
    // Could use something like Combine or an array of delegates here if multiple parties interested
    // In this case, delegate weakly held is a quick and easy solution
    var delegate: AsyncListModelDelegate? { get set}
    // State to let holder know what our status is.  If no data, is it because we're loading/error?
    var state: AsyncListModelState { get }
    // Name of the view controller shown on nav bar
    var listTitle: String { get }
}



class AsyncListTableViewController: UIViewController, AsyncListModelDelegate, UITableViewDataSource, UITableViewDelegate {

    private var model: AsyncListModel

    private var tableView: UITableView
    private var loadingView = AsyncLoadingOverlayViewController()

    private let cellID = "basicCellIdentifier"

    init(model: AsyncListModel) {
        self.model = model
        tableView = UITableView(frame: .zero)
        super.init(nibName: nil, bundle: nil)
        self.model.delegate = self
        self.title = model.listTitle
    }

    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }

    func dataUpdated() {
        tableView.reloadData()
        if model.state == .loading {
            loadingView.showAndStart()
        } else {
            loadingView.stopAndHide()
        }
    }

    override func viewDidLoad() {
        super.viewDidLoad()
        tableView.translatesAutoresizingMaskIntoConstraints = false
        view.addSubview(tableView)

        tableView.delegate = self
        tableView.dataSource = self

        NSLayoutConstraint.activate([
            tableView.leadingAnchor.constraint(equalTo: view.safeAreaLayoutGuide.leadingAnchor),
            tableView.trailingAnchor.constraint(equalTo: view.safeAreaLayoutGuide.trailingAnchor),
            tableView.topAnchor.constraint(equalTo: view.safeAreaLayoutGuide.topAnchor),
            tableView.bottomAnchor.constraint(equalTo: view.safeAreaLayoutGuide.bottomAnchor)
        ])
        tableView.register(SubtitleStyleCell.self, forCellReuseIdentifier: cellID)

        view.addSubview(loadingView.view)
        loadingView.view.translatesAutoresizingMaskIntoConstraints = false
        NSLayoutConstraint.activate([
            loadingView.view.leadingAnchor.constraint(equalTo: view.safeAreaLayoutGuide.leadingAnchor),
            loadingView.view.trailingAnchor.constraint(equalTo: view.safeAreaLayoutGuide.trailingAnchor),
            loadingView.view.topAnchor.constraint(equalTo: view.safeAreaLayoutGuide.topAnchor),
            loadingView.view.bottomAnchor.constraint(equalTo: view.safeAreaLayoutGuide.bottomAnchor)
        ])

        loadingView.showAndStart()

        model.getDataAsync()
    }

    // I generally like putting these in an extension or different file, but simplicity rules here
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        switch model.state {
        case .hasNotTriedToLoad, .loading:
            // Three dummy cells while loading just for appearance
            return 3
        case .errorLoading:
            // Error cell
            return 1
        case .dataLoaded:
            // Loaded data, or a single cell saying "No Data"
            return model.data.count > 0 ? model.data.count : 1
        }
    }

    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        guard let cell = tableView.dequeueReusableCell(withIdentifier: cellID) else {
            print("WOULD PUT AN ERROR HERE, NOT JUST A PRINT STATEMENT")
            return UITableViewCell()
        }

        // This is a bit hacky -- typically I'd use a different cell as my "dummy" to show while loading
        // For now I just use a regular cell with gray text box backgrounds
        // But that means I need to ensure they're reset when I don't want them gray!
        cell.textLabel?.backgroundColor = .white
        cell.detailTextLabel?.backgroundColor = .white
        cell.textLabel?.textColor = .black
        cell.detailTextLabel?.textColor = .black

        cell.accessoryType = .none

        switch model.state {
        case .hasNotTriedToLoad, .loading:
            // Dummy cells with no text, just gray background for text boxes
            cell.textLabel?.textColor = .clear
            cell.detailTextLabel?.textColor = .clear
            cell.textLabel?.backgroundColor = .lightGray
            cell.detailTextLabel?.backgroundColor = .lightGray
            cell.textLabel?.text = "000000000"
            cell.detailTextLabel?.text = "00000000000000000000000000000"
        case .errorLoading:
            // Usually would want a localized string, or one pulled from a model / file.
            // Simplest solution here
            cell.textLabel?.text = "Error Loading Data, Please try again"
            cell.detailTextLabel?.text = ""
        case .dataLoaded:
            if model.data.isEmpty {
                cell.textLabel?.text = "No Data Found!"
                cell.detailTextLabel?.text = ""
            } else {
                var data: ListTableCellData = BasicListTableCellData(title: "", subTitle: "")
                if indexPath.row < model.data.count {
                    data = model.data[indexPath.row]
                }
                cell.textLabel?.text = data.title
                cell.detailTextLabel?.text = data.subTitle
                cell.accessoryType = .detailButton
            }
        }
        return cell
    }

    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        tableView.deselectRow(at: indexPath, animated: false)
        guard model.data.count > indexPath.row else {
            return
        }
        let data = model.data[indexPath.row]
        // Could use a separate handler that's injected in to keep this very generic, for now just going with hard-coded
        if let viewController = NYCSchoolDisplayViewController(cellData: data) {
            navigationController?.pushViewController(viewController, animated: true)
        } else {
            print("Could not initialize detail VC, model not of right format")
        }
    }

}

class SubtitleStyleCell: UITableViewCell {
    override init(style: UITableViewCell.CellStyle, reuseIdentifier: String?) {
        super.init(style: .subtitle, reuseIdentifier: reuseIdentifier)
    }

    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
}

