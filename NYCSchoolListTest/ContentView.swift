//
//  ContentView.swift
//  NYCSchoolListTest
//
//  Created by Zac Long on 7/30/23.
//

import SwiftUI

struct ContentView: View {
    var body: some View {
        NavigationStack {
            ZStack {
                Image("NYCSchoolImg")
                    .resizable()
                    .aspectRatio(contentMode: .fill)
                VStack {
                    Spacer()
                    NavigationLink {
                        AsyncListTVCView()
                    } label: {
                        ZStack {
                            Color.white
                            Text("Tap to see NYC School List")
                                .foregroundColor(.black)
                                .padding(EdgeInsets(top: 4.0, leading: 12.0, bottom: 4.0, trailing: 12.0))
                        }
                        .frame(height: 100.0)
                    }
                }
            }
        }
    }
}

struct AsyncListTVCView: UIViewControllerRepresentable {
    func makeUIViewController(context: Context) -> AsyncListTableViewController {
        AsyncListTableViewController(model: NYCSchoolAsyncListModel())
    }

    func updateUIViewController(_ uiViewController: AsyncListTableViewController, context: Context) {
        // NOP
    }

    typealias UIViewControllerType = AsyncListTableViewController


}

struct ContentView_Previews: PreviewProvider {
    static var previews: some View {
        ContentView()
    }
}
