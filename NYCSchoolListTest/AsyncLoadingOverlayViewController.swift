//
//  AsyncLoadingOverlayViewController.swift
//  NYCSchoolListTest
//
//  Created by Zac Long on 7/30/23.
//

import Foundation
import UIKit

class AsyncLoadingOverlayViewController: UIViewController {

    private let activityIndicator = UIActivityIndicatorView(style: .large)

    override func viewDidLoad() {
        super.viewDidLoad()
        // Just using a mostly transparent gray, rather than the more sophisticated blur effect
        view.backgroundColor = .black.withAlphaComponent(0.2)
        view.addSubview(activityIndicator)
        activityIndicator.translatesAutoresizingMaskIntoConstraints = false
        NSLayoutConstraint.activate([
            activityIndicator.centerXAnchor.constraint(equalTo: view.centerXAnchor),
            activityIndicator.centerYAnchor.constraint(equalTo: view.centerYAnchor)
        ])
        self.view.isHidden = true
    }

    func showAndStart() {
        guard self.view.isHidden else {
            return
        }
        self.view.isHidden = false
        self.activityIndicator.startAnimating()
    }

    func stopAndHide() {
        guard !self.view.isHidden else {
            return
        }
        self.view.isHidden = true
        self.activityIndicator.stopAnimating()
    }
}
