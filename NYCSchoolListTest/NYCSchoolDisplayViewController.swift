//
//  NYCSchoolDisplayViewController.swift
//  NYCSchoolListTest
//
//  Created by Zac Long on 7/31/23.
//

import Foundation
import UIKit

class NYCSchoolDisplayViewController: UIViewController {
    var model: NYCSchoolData

    @IBOutlet weak var schoolNameLabel: UILabel!
    @IBOutlet weak var schoolDescriptionLabel: UILabel!

    @IBOutlet weak var schoolPhoneLabel: UILabel!
    @IBOutlet weak var schoolEmailLabel: UILabel!

    @IBOutlet weak var writingSATScoreLabel: UILabel!
    @IBOutlet weak var readingSATScoreLabel: UILabel!
    @IBOutlet weak var mathSATScoreLabel: UILabel!

    convenience init?(cellData: ListTableCellData) {
        guard let model = cellData as? NYCSchoolData else {
            return nil
        }
        self.init(model: model)
    }

    init(model: NYCSchoolData) {
        // Using a xib for the heck of it, often I prefer programmatic, but going for variety.
        // Would also usually use a model for all the hard-coded values, but speed is king today
        self.model = model
        super.init(nibName: "NYCSchoolDisplayViewController", bundle: .main)
        title = "Details"
    }

    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }

    override func viewDidLoad() {
        super.viewDidLoad()

        // Lay out the details.  We want to show all SAT scores as well as a few other things
        schoolNameLabel.text = model.school_name
        schoolPhoneLabel.text = model.phone_number ?? "No Phone Number Listed"
        schoolEmailLabel.text = model.school_email ?? "No Email Listed"
        schoolDescriptionLabel.text = model.overview_paragraph ?? ""

        readingSATScoreLabel.text = model.sat_critical_reading_avg_score ?? "Unknown"
        mathSATScoreLabel.text = model.sat_math_avg_score ?? "Unknown"
        writingSATScoreLabel.text = model.sat_writing_avg_score ?? "Unknown"
    }
}
