//
//  NYCSchoolListTestApp.swift
//  NYCSchoolListTest
//
//  Created by Zac Long on 7/30/23.
//

import SwiftUI

@main
struct NYCSchoolListTestApp: App {
    var body: some Scene {
        WindowGroup {
            ContentView()
        }
    }
}
